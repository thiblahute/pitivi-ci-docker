FROM base/archlinux
MAINTAINER Thibault Saunier <tsaunier@gnome.org>

RUN pacman -Sy --noconfirm flatpak-builder git xorg-server-xvfb
RUN flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
RUN flatpak install flathub org.gnome.Sdk//3.24
RUN flatpak install flathub org.gnome.Platform//3.24
